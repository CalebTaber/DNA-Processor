This is a simple program designed to take eukaryotic DNA sequences and convert them into mRNA and amino acid sequences.

The data collected from this program is not 100% accurate, as RNA processing and gene expression are very complex processes, but for most intents and purposes, these data are accurate enough.

You can download various genomes from this website: ftp://ftp.ncbi.nih.gov/genomes/
