package main;

import java.io.*;

public class Main {

    private static String cNum = "Y"; // Change this

    public static void main(String args[]) {
        String path = "/home/caleb/Documents/HG/" + cNum + "/";
        File log = new File(path + "C" + cNum + "_STATS.txt");

        DNAtoMRNA(path, log);
        MRNAtoAA(path, log);
        System.out.println("Done with chromosome " + cNum);
    }

    private static void DNAtoMRNA(String path, File log) {
        try {
            BufferedWriter stats = new BufferedWriter(new FileWriter(log));

            long a, t, c, g, total;
            a = t = c = g = 0;

            // DNA to pre-mRNA
            BufferedReader in = new BufferedReader(new FileReader(path + "C" + cNum + "_DNA.txt"));
            BufferedWriter out = new BufferedWriter(new FileWriter(path + "C" + cNum + "_preMRNA.txt"));

            int lineLength = 0;
            while (in.ready()) {
                lineLength++;
                int i = in.read();

                if (i == 65) { // If A, write U
                    out.write('U');
                    a++;
                } else if (i == 84) { // If T, write A
                    out.write('A');
                    t++;
                } else if (i == 67) { // If C, write G
                    out.write('G');
                    c++;
                } else if (i == 71) { // If G, write C
                    out.write('C');
                    g++;
                }

                if (lineLength == 70) {
                    out.write("\n");
                    lineLength = 0;
                }
            }

            total = a + t + c + g;
            double ap = (double) a / (double) total;
            double tp = (double) t / (double) total;
            double cp = (double) c / (double) total;
            double gp = (double) g / (double) total;
            stats.write("Adenine:" + a + ":" + ap + "\nThymine:" + t + ":" + tp + "\nCytosine:" + c + ":" + cp + "\nGuanine:" + g + ":" + gp + "\nTotal:" + total + "\n\n");
            stats.close();
            in.close();
            out.close();

            // pre-mRNA to mRNA
            in = new BufferedReader(new FileReader(path + "C" + cNum + "_preMRNA.txt"));
            out = new BufferedWriter(new FileWriter(path + "C" + cNum + "_MRNA.txt"));

            // Find exons
            boolean isExon = false;
            StringBuilder exon = new StringBuilder();
            while (in.ready()) {
                int one = in.read();
                if (one == 10) one = in.read();
                int two = in.read();
                if (two == 10) two = in.read();
                int three = in.read();
                if (three == 10) three = in.read();
                char[] codon = new char[]{asciiToRNA(one), asciiToRNA(two), asciiToRNA(three)};

                String s = charArrToString(codon);
                if(!isExon && s.equals("AUG")) { // If a start codon is encountered
                    exon.append(s);
                    isExon = true;
                } else if (isExon && (s.equals("UGA") || s.equals("UAA") || s.equals("UAG"))) { // If stop codon is encountered
                    exon.append("\n"); // Separate exons by a newline
                    out.write(exon.toString());
                    exon = new StringBuilder();
                    isExon = false;
                } else if (isExon) exon.append(s);
            }

            in.close();
            out.close();
        } catch (IOException e ) {
            e.printStackTrace();
        }
    }

    private static void MRNAtoAA(String path, File log) {
        try {
            BufferedWriter stats = new BufferedWriter(new FileWriter(log, true));

            // RNA to AA
            BufferedReader in = new BufferedReader(new FileReader(path + "C" + cNum + "_MRNA.txt"));
            BufferedWriter out = new BufferedWriter(new FileWriter(path + "C" + cNum + "_AA.txt"));
            int lineLength = 0;

            long ala, arg, asn, asp, cys, glu, gln, gly, his, ile, leu, lys, met, phe, pro, ser, thr, trp, tyr, val, total;
            total = ala = arg = asn = asp = cys = glu = gln = gly = his = ile = leu = lys = met = phe = pro = ser = thr = trp = tyr = val = 0;

            while (in.ready()) {
                lineLength += 5;
                int one = in.read();
                if (one == 10) one = in.read();
                int two = in.read();
                if (two == 10) two = in.read();
                int three = in.read();
                if (three == 10) three = in.read();
                char[] codon = new char[]{asciiToRNA(one), asciiToRNA(two), asciiToRNA(three)};

                if (codon[0] == 'A') {
                    if (codon[1] == 'A') {
                        if (codon[2] == 'A' || codon[2] == 'G') {
                            out.write("LYS, ");
                            lys++;
                        } else if (codon[2] == 'U' || codon[2] == 'C') {
                            out.write("ASN, ");
                            asn++;
                        }
                    } else if (codon[1] == 'U') {
                        if (codon[2] == 'A' || codon[2] == 'U' || codon[2] == 'C') {
                            out.write("ILE, ");
                            ile++;
                        } else if (codon[2] == 'G') {
                            out.write("MET, ");
                            met++;
                        }
                    } else if (codon[1] == 'C') {
                        out.write("THR, ");
                        thr++;
                    } else if (codon[1] == 'G') {
                        if (codon[2] == 'A' || codon[2] == 'G') {
                            out.write("ARG, ");
                            arg++;
                        } else if (codon[2] == 'U' || codon[2] == 'C') {
                            out.write("SER, ");
                            ser++;
                        }
                    }
                } else if (codon[0] == 'U') {
                    if (codon[1] == 'A') {
                        if (codon[2] == 'A' || codon[2] == 'G') {
                            out.write("STOP, ");
                        } else if (codon[2] == 'U' || codon[2] == 'C') {
                            out.write("TYR, ");
                            tyr++;
                        }
                    } else if (codon[1] == 'U') {
                        if (codon[2] == 'A' || codon[2] == 'G') {
                            out.write("LEU, ");
                            leu++;
                        } else if (codon[2] == 'U' || codon[2] == 'C') {
                            out.write("PHE, ");
                            phe++;
                        }
                    } else if (codon[1] == 'C') {
                        out.write("SER, ");
                        ser++;
                    } else if (codon[1] == 'G') {
                        if (codon[2] == 'A') {
                            out.write("STOP, ");
                        } else if (codon[2] == 'U' || codon[2] == 'C') {
                            out.write("CYS, ");
                            cys++;
                        } else if (codon[2] == 'G') {
                            out.write("TRP, ");
                            trp++;
                        }
                    }
                } else if (codon[0] == 'C') {
                    if (codon[1] == 'A') {
                        if (codon[2] == 'A' || codon[2] == 'G') {
                            out.write("GLN, ");
                            gln++;
                        } else if (codon[2] == 'U' || codon[2] == 'C') {
                            out.write("HIS, ");
                            his++;
                        }
                    } else if (codon[1] == 'U') {
                        out.write("LEU, ");
                        leu++;
                    } else if (codon[1] == 'C') {
                        out.write("PRO, ");
                        pro++;
                    } else if (codon[1] == 'G') {
                        out.write("ARG, ");
                        arg++;
                    }
                } else if (codon[0] == 'G') {
                    if (codon[1] == 'A') {
                        if (codon[2] == 'A' || codon[2] == 'G') {
                            out.write("GLU, ");
                            glu++;
                        } else if (codon[2] == 'U' || codon[2] == 'C') {
                            out.write("ASP, ");
                            asp++;
                        }
                    } else if (codon[1] == 'U') {
                        out.write("VAL, ");
                        val++;
                    } else if (codon[1] == 'C') {
                        out.write("ALA, ");
                        ala++;
                    } else if (codon[1] == 'G') {
                        out.write("GLY, ");
                        gly++;
                    }
                }

                if (lineLength == 70) {
                    out.write("\n");
                    lineLength = 0;
                }
            }

            // Tabulate the frequencies and percentages of the amino acids
            String[] aminoAcids = new String[]{"Alanine", "Arginine", "Asparagine", "Aspartic Acid", "Cytesine", "Glutamic Acid", "Glutamine", "Glycine", "Histidine", "Isoleucine",
            "Leucine", "Lysine", "Methionine", "Phenylalanine", "Proline", "Serine", "Threonine", "Tryptophan", "Tyrosine", "Valine"};
            long[] freq = new long[] {ala, arg, asn, asp, cys, glu, gln, gly, his, ile, leu, lys, met, phe, pro, ser, thr, trp, tyr, val};
            for (long l : freq) {
                total += l;
            }

            double[] percents = new double[20];
            for (int i = 0; i < freq.length; i++) {
                percents[i] = (double) freq[i] / (double) total;
            }

            for (int i = 0; i < aminoAcids.length; i++) {
                stats.write(aminoAcids[i] + ":" + freq[i] + ":" + percents[i] + "\n");
            }

            stats.write("Total:" + total);

            stats.close();
            in.close();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String charArrToString(char[] arr) {
        StringBuilder s = new StringBuilder();
        for (char c : arr) {
            s.append(c);
        }

        return s.toString();
    }

    private static char asciiToRNA(int i) {
        if (i == 65) return 'A';
        else if (i == 67) return 'C';
        else if (i == 71) return 'G';
        else if (i == 85) return 'U';
        return ' ';
    }

}

